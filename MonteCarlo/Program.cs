﻿using Helpers;

Dictionary<int, int> MonteCarlo(Func<int> func, int count)
{
    return Enumerable.Range(0, count)
        .Select(_ => func())
        .GroupBy(x => x)
        .ToDictionary(x => x.Key, x => x.Count());
}

Random rnd = Random.Shared;

int DiceRoll(int sides = 6) => Random.Shared.Next(sides - 1) + 1;
int DiceRollFast(int sides = 6) => rnd.Next(sides - 1) + 1;

int MultipleDiceRoll(int cubes, int sides = 6, bool fast = false) => fast
    ? Enumerable.Range(0, cubes).Sum(_ => DiceRollFast(sides))
    : Enumerable.Range(0, cubes).Sum(_ => DiceRoll(sides));

bool CoinFlip() => Random.Shared.Next(2) == 1;
bool CoinFlipFast() => rnd.Next(2) == 1;

IEnumerator<bool> MultipleCoinFlip(int flips, bool fast = false) => fast
    ? Enumerable.Range(0, flips).Select(_ => CoinFlipFast()).GetEnumerator()
    : Enumerable.Range(0, flips).Select(_ => CoinFlip()).GetEnumerator();

const int tests = 1_000_000;
const int tosses = 7;

// slow version
{
    var (time, results) = WithTime.TimeFunction(() =>
        MonteCarlo(() => ContainsConsecutiveTosses(MultipleCoinFlip(100), tosses) ? 1 : 0, tests));
    Console.WriteLine(
        $"Chance of rolling {tosses} consecutive heads in 100 rolls is {(double)results[1] / tests * 100}%");
    Console.WriteLine($"Test took {time.TotalMilliseconds}ms");
}

// fast version
{
    var (time, results) = WithTime.TimeFunction(() =>
        MonteCarlo(() => ContainsConsecutiveTosses(MultipleCoinFlip(100, true), tosses) ? 1 : 0, tests));
    Console.WriteLine(
        $"Chance of rolling {tosses} consecutive heads in 100 rolls is {(double)results[1] / tests * 100}%");
    Console.WriteLine($"Test took {time.TotalMilliseconds}ms");
}

bool ContainsConsecutiveTosses(IEnumerator<bool> super, int tossCount)
{
    int count = 0;
    super.MoveNext();

    while (true)
    {
        if (super.Current)
            count++;
        else
            count = 0;

        if (count == tossCount) return true;
        if (!super.MoveNext()) return false;
    }
}