﻿var numbersSet = new HashSet<int>(50);

while (numbersSet.Count < 50)
    numbersSet.Add(Random.Shared.Next(100));

var numbers = numbersSet.ToArray();

for (int i = 1; i < numbers.Length; i += 2)
    Console.WriteLine($"{i}: {numbers[i]}");

Console.WriteLine(new string('-', 50));

for (int i = 0; i < numbers.Length; i += 2)
    Console.WriteLine($"{i}: {numbers[i]}");

