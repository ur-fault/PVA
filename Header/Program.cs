﻿string TrimToLenght(string text, int lenght) {
    if (text.Length > lenght)
        return $"{text.Substring(0, lenght - 3)}...";

    return text;
}

void PrintHeader(int width, int height, string title, char[]? blockCharsNullable = null) {
    var blockChars = blockCharsNullable ?? new[] { '-', '|', '+' };
    if (blockChars.Length != 3)
        throw new ArgumentException("blockChars must have 3 elements", nameof(blockCharsNullable));

    if (width < 3)
        throw new ArgumentException("width must be at least 3", nameof(width));

    if (height < 4)
        throw new ArgumentException("height must be at least 4", nameof(width));

    title = TrimToLenght(title, width - 2);

    var horizontal = blockChars[0];
    var vertical = blockChars[1];
    var corner = blockChars[2];

    var titleLine = height % 2 == 0 ? (height - 2) / 2 - 1 : (height - 2) / 2;

    // print top line
    Console.WriteLine($"{corner}{new(horizontal, width - 2)}{corner}");

    // print box
    for (int line = 0; line < height - 2; line++) {
        if (line == titleLine) {
            var left = (width - 2 - title.Length) / 2;
            var right = width - 2 - title.Length - left;
            
            Console.WriteLine($"{vertical}{new(' ', left)}{title}{new(' ', right)}{vertical}");
        }
        else if (line == height - 3) {
            // get today
            var todayString = DateTime.Now.ToLongDateString();
            
            // date is too big to be properly displayed
            if (todayString.Length > width - 3)
                todayString = TrimToLenght(DateTime.Now.ToShortDateString(), width - 3);

            Console.WriteLine($"{vertical}{todayString.PadLeft(width - 3)} {vertical}");
        }
        else
            Console.WriteLine($"{vertical}{new(' ', width - 2)}{vertical}");
    }

    // print bottom line
    Console.WriteLine($"{corner}{new(horizontal, width - 2)}{corner}");
}

PrintHeader(100, 11, "title, test", new []{ 'x', 'y', 'z' });