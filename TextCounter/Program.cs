﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;


// string substring = @"watashi";

// Console.WriteLine($"Number of occurrences of '{substring}' in original is {Counter.CountSubstrings(original, substring)}");

BenchmarkRunner.Run<Counter>();

[MemoryDiagnoser]
class Counter
{
    private const string Original = @"Nagarete ku toki no naka de demo

kedaru-sa ga hora guruguru mawatte
watashi kara hanareru kokoro mo
mienai wa so shiranai
jibun kara ugoku koto mo naku
toki no sukima ni nagasare tsuzukete
shiranai wa mawari no koto nado
watashi wa watashi sore dake

yumemi teru? Nani mo mitenai?
Kataru mo mudana jibun no kotoba
kanashimu nante tsukareru dake yo
nani mo kanjizu sugoseba i no
tomadou kotoba atae rarete mo

jibun no kokoro tada uwanosora
moshi watashi kara ugoku nonaraba
subete kaeru nonara kuro ni suru

kon'na jibun ni mirai wa aru no?

Kon'na sekai ni watashi wa iru no?
Ima setsunai no? Ima kanashi no?
Jibun no koto mo wakaranai mama
ayumu koto sae tsukareru dake yo
hito no koto nado shiri mo shinai wa
kon'na watashi mo kawareru nonara
moshi kawareru no nara shiro ni naru


nagare teku toki no naka de demo

kedaru-sa ga hora guruguru mawatte
watashi kara hanareru kokoro mo
mienai wa so shiranai
jibun kara ugoku koto mo naku

toki no sukima ni nagasa re tsudzukete

shiranai wa mawari no koto nado
watashi wa watashi soredake

ima yumemi teru? Nani mo mitenai?

Kataru mo mudana jibun no kotoba

kanashimu nante tsukareru dake yo
nani mo kanjizu sugoseba i no
tomadou kotoba atae rarete mo
jibun no kokoro tada uwanosora
moshi watashi kara ugoku nonaraba
subete kaeru nonara kuro ni suru

ugoku nonaraba ugoku nonaraba
subete kowasu wa subete kowasu wa
kanashimunaraba kanashimunaraba
watashi no kokoro shiroku kawareru?
Anata no koto mo watashi no koto mo
subete no koto mo mada shiranai no
omoi mabuta o aketa nonaraba

subete kowasu nonara kuro ni nare! !";

    private const string Substring = "watashi";

    public int CountSubstrings_Span(string original, string substring) {
        unsafe {
            var originalSpan = original.AsSpan();
            var substringSpan = substring.AsSpan();

            var originalLength = originalSpan.Length;
            var substringLength = substringSpan.Length;

            fixed (char* originalPtr = originalSpan, substringPtr = substringSpan) {
                return Enumerable.Range(0, originalSpan.Length).Count(i =>
                    new Span<Char>(originalPtr, originalLength).Slice(i)
                        .StartsWith(new Span<Char>(substringPtr, substringLength)));
            }
        }
    }
    
    public int CountSubstrings(string original, string substring) {
        return Enumerable.Range(0, original.Length).Count(i =>
            original.Substring(i).StartsWith(substring));
    }
    
    [Benchmark]
    public void Counter_Span() {
        CountSubstrings_Span(Original, Substring);
    }
    
    [Benchmark]
    public void Counter_String() {
        CountSubstrings(Original, Substring);
    }
}