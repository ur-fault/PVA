﻿using System.Text.RegularExpressions;

Regex userRegex = new("^[a-zA-Z0-9_]+$");
Regex pinRegex = new(@"^\d{4}$");

void Beep(int count) {
    async void BeepAsync() {
        foreach (var _ in Enumerable.Range(0, count)) {
            Console.Beep();
            await Task.Delay(400);
        }
    }

    new Task(BeepAsync).Start();
}

(bool, bool) CheckLogin(string user, string pin) => (
    userRegex.IsMatch(user), pinRegex.IsMatch(pin)
);

while (true) {
    Console.Write("Username: ");
    string user = Console.ReadLine()!;
    Console.Write("PIN: ");
    string pin = Console.ReadLine()!;

    var res = CheckLogin(user, pin);

    var (msg, beepCount) = res switch {
        (true, true) => ("Login successful", 0),
        (true, false) => ("Pin is invalid", 1),
        (false, true) => ("User is invalid", 2),
        (false, false) => ("User and pin are invalid", 3)
    };
    
    Console.WriteLine(msg);
    Beep(beepCount);
}