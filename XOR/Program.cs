﻿using System.Text;

void EncryptDecryptFile(string path, string key) {
    const int maxBufferSize = 1024;
    // get buffer size that is divisible by key length
    var bufferSize = maxBufferSize - maxBufferSize % key.Length;

    // will be disposed at the end of this block
    using var r = new StreamReader(path);
    using var w = new StreamWriter(path + ".cry");

    // init buffers
    var keyBytes = Encoding.ASCII.GetBytes(key);
    var buffer = new char[bufferSize];
    var bufferBytes = new byte[bufferSize];
    
    int read;

    while ((read = r.ReadBlock(buffer, 0, buffer.Length)) > 0) {
        Encoding.ASCII.GetBytes(
            new ReadOnlySpan<char>(buffer, 0, read),
            new Span<byte>(bufferBytes, 0, read));

        for (int i = 0; i < read; i++)
            bufferBytes[i] = (byte)(keyBytes[i % keyBytes.Length] ^ bufferBytes[i]);

        w.Write(Encoding.ASCII.GetChars(bufferBytes), 0, read);
    }
}

Console.Write("Enter path to file: ");
string path = Console.ReadLine() ?? throw new Exception("Something went wrong");
Console.Write("Enter key (max 1024 chars): ");
string key = Console.ReadLine() ?? throw new Exception("Something went wrong");
EncryptDecryptFile(path, key);
EncryptDecryptFile(path + ".cry", key);