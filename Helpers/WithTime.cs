using System.Diagnostics;

namespace Helpers;

public static class WithTime
{
    public static (TimeSpan, T) TimeFunction<T>(Func<T> func)
    {
        var stopWatch = new Stopwatch();
        stopWatch.Start();
        var res = func();
        stopWatch.Stop();
        return (stopWatch.Elapsed, res);
    }
}