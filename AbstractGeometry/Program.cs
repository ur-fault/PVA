﻿var objects = new AbstractGeometryObject[] {
    new Circle(5f),
    new Square(4f),
    new Circle { X = 5f, Y = 5f, Radius = 5f },
    new Square { X = 5f, Y = 5f, Side = 420f }
}; 

abstract class AbstractGeometryObject
{
    private (float, float) _position;
    public (float, float) Position
    {
        get => _position;
        set => _position = value;
    }
    
    public float X
    {
        get => Position.Item1;
        set => _position.Item1 = value;
    }

    public float Y
    {
        get => Position.Item2;
        set => _position.Item2 = value;
    }
    public Guid Id { get; } = Guid.NewGuid();


    public abstract float Area();
    public abstract float Perimeter();

    public float MomentX() => Math.Abs(X) * Area();

    public AbstractGeometryObject() {
        Position = (0, 0);
    }
    public AbstractGeometryObject((float, float) position) {
        Position = position;
    }
}

class Square : AbstractGeometryObject
{
    public float Side { get; set; }

    public override float Area() => Side * Side;
    public override float Perimeter() => 4 * Side;

    public Square() {
        Side = 1f;
    }

    public Square(float side) {
        Side = side;
    }
}

class Circle : AbstractGeometryObject
{
    public float Radius { get; set; }

    public override float Area() => (float)Math.PI * Radius * Radius;
    public override float Perimeter() => 2 * (float)Math.PI * Radius;

    public Circle() {
        Radius = 1f;
    }

    public Circle(float radius) {
        Radius = radius;
    }
}