﻿using System.Text.RegularExpressions;

var mailRegex = new Regex(@"^(\w+\.)*\w@(\w+\.)+[a-zA-Z]{2,4}$");

bool CheckMail(string mail) => mailRegex.IsMatch(mail);

void PrintCheckMail(string mail) {
    Console.WriteLine($"{mail} is {(CheckMail(mail) ? "valid" : "invalid")}");
}

PrintCheckMail("foo@bar.com");
PrintCheckMail("foo@bar");
PrintCheckMail("foo2.bar@foo.com");
PrintCheckMail("foo2.bar@foo.bar.com");
PrintCheckMail("foo_bar@com.bar");