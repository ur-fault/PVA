﻿using System.Collections.Generic;
using System.Text;

if (args.Length == 0) {
    Console.WriteLine($"Usage: {AppDomain.CurrentDomain.FriendlyName} <filename>");
    return;
}

var text = File.ReadAllText(args[0]);
if (text is not { }) {
    Console.WriteLine("File is empty");
    return;
}

var splitChars = new[] { ' ', '\t', '\r', ',', '.', '\n' };
var index = new WordIndex();

StringBuilder currentWord = new();
int wordStart = 0;
int line = 1;
int column = 0;

var i = 0;
foreach (var chr in text) {
    if (splitChars.Contains(chr)) {
        if (currentWord.Length > 2) {
            var word = currentWord.ToString();
            index.AddEntry(word, new(wordStart, line, column));
            currentWord.Clear();

            wordStart = i + 1;
        }

        if (chr == '\n') {
            line++;
            column = 0;
        }
    }
    else
        currentWord.Append(chr);

    column++;
    i++;
}

Console.WriteLine("Index:");

// make sorted index
var sortedIndex = index.Entries.ToList();
sortedIndex.Sort();

// print sorted index
foreach (var entry in sortedIndex)
    Console.WriteLine($"{entry.Word}: {string.Join(", ", entry.Positions.Select(pos => $"({pos})"))}");

// prepare for index for saving to file
List<string> lines = new(sortedIndex.Count * 4) {"", "Index"};
foreach (var entry in sortedIndex) {
    lines.Add($"{entry.Word}, {entry.Positions.Count}x");
    
    foreach (var pos in entry.Positions)
        lines.Add($"\t{pos}");
}

// save to file
File.AppendAllLines(args[0], lines);

class WordIndex
{
    private readonly Dictionary<string, List<Position>> _wordIndex = new();

    public void AddEntry(string word, Position pos) {
        if (_wordIndex.ContainsKey(word))
            _wordIndex[word].Add(pos);
        else
            _wordIndex.Add(word, new List<Position> { pos });
    }

    public IEnumerable<Entry> Entries => _wordIndex.Select(x => new Entry(x.Key, x.Value));

    internal record Entry(string Word, List<Position> Positions) : IComparable<Entry>
    {
        public int CompareTo(Entry? other) {
            return String.Compare(Word, other?.Word, StringComparison.Ordinal);
        }
    }

    internal readonly record struct Position(int Pos, int Line, int Column)
    {
        public override string ToString() => $"char {Pos}, line {Line}, col {Column}";
    }
}