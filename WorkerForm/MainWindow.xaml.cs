﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Documents;

namespace WorkerForm;
/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
    private int _errors;
    public int Errors => _errors;

    public MainWindow() {
        InitializeComponent();

        DataContext = new Worker();

        BirthYearComboBox.ItemsSource = Enumerable.Range(1950, DateTime.Today.Year - 1950)
            .Cast<object>()
            .Reverse()
            .Prepend("Choose the year")
            .ToList();
        BirthYearComboBox.SelectedIndex = 0;

        EducationLevelComboBox.SelectedIndex = 0;

        JobPositionComboBox.SelectedIndex = 0;
    }

    private async Task SaveWorker() {
        var worker = (Worker)DataContext;

        var workers =
            await JsonSerializer.DeserializeAsync<List<dynamic>>(
                new MemoryStream(Encoding.UTF8.GetBytes(File.Exists("Workers.json")
                    ? await File.ReadAllTextAsync("workers.json")
                    : "[]")));

        workers ??= new();

        workers.Add(worker);
        var json = JsonSerializer.Serialize(workers);
        await File.WriteAllTextAsync("workers.json", json);
    }

    private void CanSave(object sender, CanExecuteRoutedEventArgs e) {
        e.CanExecute = _errors == 0;
        e.Handled = true;
    }

    private async void Save(object sender, ExecutedRoutedEventArgs e) {
        await SaveWorker();
        e.Handled = true;
    }

    private void HandleValidationError(object? sender, ValidationErrorEventArgs e) {
        if (e.Action == ValidationErrorEventAction.Added)
            _errors++;
        else
            _errors--;

        ErrorCounterRun?.GetBindingExpression(Run.TextProperty)?.UpdateTarget();
    }
}