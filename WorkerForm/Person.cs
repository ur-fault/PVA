﻿using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.Json.Serialization;

namespace WorkerForm;

public class Person : INotifyPropertyChanged, IDataErrorInfo
{
    private string _firstName = string.Empty;
    private string _lastName = string.Empty;
    private int _birthYear;

    [NoWhitespaceFilter]
    [NotEmptyFilter]
    public string FirstName {
        get => _firstName;
        set {
            _firstName = value;
            OnChangedProperty();
        }
    }

    [NotEmptyFilter]
    public string LastName {
        get => _lastName;
        set {
            _lastName = value;
            OnChangedProperty();
        }
    }

    [MinimalAgeFilter(18, valueIsYear: true)]
    public int BirthYear {
        get => _birthYear;
        set {
            _birthYear = value;
            OnChangedProperty();
        }
    }


    public event PropertyChangedEventHandler? PropertyChanged;

    protected void OnChangedProperty([CallerMemberName] string? name = null) {
        if (name == null) return;

        //MessageBox.Show($"{name} changed value to {this.GetType().GetProperty(name)?.GetValue(this)}");
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
    }

    [JsonIgnore]
    public string Error { get; } = "";

    public string? this[string columnName] {
        get {
            var value = GetType().GetProperty(columnName)?.GetValue(this);

            // Find all attributes that report errors
            var errors = GetType()
                .GetProperty(columnName)
                ?.GetCustomAttributes(typeof(ValueFilterAttribute),
                    true)
                .Cast<ValueFilterAttribute>()
                .Select(attr => attr.Check(value))
                .Where(error => error is not null)
                .Select(error => error!)
                .ToList();

            return (errors is null || errors.Count == 0 ? null : string.Join(", ", errors)) ?? string.Empty;
        }
    }
}