﻿using System;

namespace WorkerForm;

public abstract class ValueFilterAttribute : Attribute
{
    /// <summary>
    /// Returns null if value is valid, otherwise string with error message
    /// </summary>
    /// <param name="value"></param>
    public abstract string? Check(object? value);
}