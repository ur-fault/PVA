﻿using System;

namespace WorkerForm;

public class MinimalAgeFilter : ValueFilterAttribute
{
    public MinimalAgeFilter(int minimalAge, bool valueIsYear = false) {
        MinimalAge = minimalAge;
        ValueIsYear = valueIsYear;
    }

    public int MinimalAge { get; set; }
    public bool ValueIsYear { get; set; }

    public override string? Check(object? value) {
        if (value is not int x) return "Value is not int";

        return ValueIsYear ? DateTime.Today.Year - x < MinimalAge ? $"Minimal age is {MinimalAge}" : null :
            x < MinimalAge ? $"Minimal age is {MinimalAge}" : null;
    }
}

public class NoWhitespaceFilter : ValueFilterAttribute
{
    public override string? Check(object? value) {
        if (value is not string x) return "Value is not string";

        return x.Contains(' ') ? "Value cannot contain whitespace" : null;
    }
}

public class NotEmptyFilter : ValueFilterAttribute
{
    public override string? Check(object? value) {
        if (value is not string x) return "Value is not string";

        return string.IsNullOrEmpty(x) ? "Value cannot be empty" : null;
    }
}

public class MinimalEducationLevelFilter : ValueFilterAttribute
{
    public MinimalEducationLevelFilter(EducationLevel minimalEducationLevel) {
        MinimalEducationLevel = minimalEducationLevel;
    }

    public EducationLevel MinimalEducationLevel { get; set; }

    public override string? Check(object? value) {
        if (value is not EducationLevel edLevel) return "Value is not string";

        return (int)edLevel >= (int)MinimalEducationLevel ? null : $"Minimal education level is {MinimalEducationLevel.Description()}";
    }
}