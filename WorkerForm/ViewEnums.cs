﻿
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System;
using System.Linq;
using static WorkerForm.EnumHelper;
using System.Windows.Data;
using System.Windows.Markup;

namespace WorkerForm;


// From https://stackoverflow.com/a/12430331
public static class EnumHelper
{
    public static string? Description(this Enum value) {
        var attributes = value.GetType()
            .GetField(value.ToString())
            ?.GetCustomAttributes(typeof(DescriptionAttribute),
                false);

        if (attributes != null && attributes.Any())
            return (attributes.First() as DescriptionAttribute)?.Description;

        var ti = CultureInfo.CurrentCulture.TextInfo;
        return ti.ToTitleCase(ti.ToLower(value.ToString().Replace("_", " ")));
    }

    public static IEnumerable<ValueDescription> GetAllValuesAndDescriptions(Type t) {
        if (!t.IsEnum)
            throw new ArgumentException($"{nameof(t)} must be an enum type");

        return Enum.GetValues(t)
            .Cast<Enum>()
            .Select((e) => new ValueDescription {
                Value = e,
                Description = e.Description()
            })
            .ToList();
    }

    public class ValueDescription
    {
        public object? Value { get; set; }
        // ReSharper disable once MemberHidesStaticFromOuterClass
        public object? Description { get; set; }
    }
}

// Also from https://stackoverflow.com/a/12430331
[ValueConversion(typeof(Enum), typeof(IEnumerable<(Enum e, string?)>))]
public class EnumToCollectionConverter : MarkupExtension, IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
        return GetAllValuesAndDescriptions(value.GetType());
    }


    public object? ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
        return null;
    }
    public override object ProvideValue(IServiceProvider serviceProvider) {
        return this;
    }
}