﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace WorkerForm;

public class Worker : Person
{
    [MinimalEducationLevelFilter(EducationLevel.HighSchool)]
    public EducationLevel EducationLevel { get; set; } = EducationLevel.None;

    public JobPosition JobPosition { get; set; } = JobPosition.None;
}


public enum EducationLevel
{
    [Description("None")]
    None,

    [Description("Elementary School")]
    ElementarySchool,

    [Description("High School")]
    HighSchool,

    [Description("College")]
    College,

    [Description("Master")]
    Master,

    [Description("PhD")]
    PhD
}

public enum JobPosition
{
    [Description("None")]
    None,

    [Description("Marketing Manager")]
    MarketingManager,

    [Description("Sales Manager")]
    SalesManager,

    [Description("Programmer")]
    Programmer,

    [Description("Project Manager")]
    ProjectManager,

    [Description("Senior Developer")]
    SeniorDeveloper,

    // ReSharper disable once InconsistentNaming
    [Description("IT Specialist")]
    ITSpecialist
}