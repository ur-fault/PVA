﻿Console.WriteLine(new Student("Kevin", "Black", 5));
class Human
{
    private string _name = null!;
    private string _lastName = null!;

    public string Name
    {
        get => _name;
        set {
            if (string.IsNullOrWhiteSpace(value))
                throw new ArgumentException("cannot be null or empty", nameof(value));
            _name = value;
        }
    }

    public string LastName
    {
        get => _lastName;
        set {
            if (string.IsNullOrWhiteSpace(value))
                throw new ArgumentException("cannot be null or empty", nameof(value));
            _lastName = value;
        }
    }

    public Human(string name, string lastName) {
        Name = name;
        LastName = lastName;
    }
}

class Student : Human
{
    public int Mark { get; set; }

    public Student(string name, string lastName, int mark) : base(name, lastName) {
        Mark = mark;
    }
}

class Worker : Human
{
    public int Salary { get; set; }
    public int HoursWorker { get; set; }

    public int MoneyPerHour() => Salary / HoursWorker;

    public Worker(string name, string lastName, int salary, int hoursWorker) : base(name, lastName) {
        Salary = salary;
        HoursWorker = hoursWorker;
    }
}