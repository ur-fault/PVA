﻿using System.Data;

var stack = new ArrayStack(20);

string test = "{2+3*[2+7*(2-3]]}";


// (char, char)[] brackets = { ('{', '}'), ('[', ']'), ('(', ')') };
Dictionary<char, char> brackets = new Dictionary<char, char>
    { { '{', '}' }, { '[', ']' }, { '(', ')' } };



for (int i = 0; i < test.Length; i++) {
    char val = test[i];
    if (brackets.ContainsKey(val))
        stack.Push(val);
    else if (brackets.ContainsValue(val)) {
        char x;
        try {
            x = stack.Pop();
        }
        catch (ArgumentOutOfRangeException) {
            PrintError(expression: test, index: i);
            throw new InvalidExpressionException($"Unmatched closing {val} at index {i}");
        }

        if (brackets[x] != val) {
            PrintError(expression: test, index: i);
            throw new InvalidExpressionException($"Mismatched brackets {x} and {val} at index {i}");
        }
    }
}

Console.WriteLine("Ok");

void PrintError(string expression, int index) {
    Console.WriteLine(expression);
    Console.WriteLine(new string(' ', index) + '^');
}

interface IStack
{
    public void Push(char ch);
    public char Pop();
    public char Peek();

    public bool IsEmpty();
    public void PrintStack() => Console.WriteLine(this);
    public int Count();
}

class ListStack : IStack
{
    readonly List<char> chars = new();

    public ListStack() { }

    public ListStack(List<char> chars) => this.chars = chars.ToList();

    public void Push(char ch) => chars.Insert(0, ch);
    public char Peek() => chars[0];

    public char Pop() {
        var ch = chars[0];
        chars.RemoveAt(0);
        return ch;
    }

    public bool IsEmpty() => Count() == 0;
    public void PrintStack() => Console.WriteLine(this);
    public int Count() => chars.Count;

    public override string ToString() => "<" + string.Join(", ", chars) + ">";
}

class ArrayStack : IStack
{
    private readonly char[] chars;
    private int top = -1;

    public ArrayStack(int size) => chars = new char[size];

    public void Push(char ch) {
        if (top == chars.Length - 1)
            throw new InvalidOperationException("Stack is full");
        
        chars[++top] = ch;
    }

    public char Pop() {
        if (top == -1)
            throw new InvalidOperationException("Stack is empty");

        return chars[top--];
    }

    public char Peek() {
        return chars[top];
    }

    public bool IsEmpty() {
        return top < 0;
    }

    public int Count() {
        return top + 1;
    }
}