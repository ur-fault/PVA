﻿using System.Text.RegularExpressions;

var objects = new List<Shape>();

// get objects from console
string input;
while ((input = Helpers.GetStringFromConsole("Enter object or empty line to continue: ")) != string.Empty) {
    try {
        var (shape, argList) = Shape.GetArgs(input);
        Console.WriteLine($"Shape: {shape}, Args: {string.Join(", ", argList)}");

        // create object
        Shape obj = shape switch {
            "circle" or "c" => new Circle().Parse(argList),
            "square" or "s" => new Square().Parse(argList),
            "rectangle" or "r" => new Rectangle().Parse(argList),
            _ => throw new ArgumentException("Unknown shape")
        };

        // check for redundant args
        if (argList.Count > 0)
            throw new ArgumentException($"Redundant arguments: {string.Join(", ", argList)}");

        Console.WriteLine($"Created object: {obj}");

        // add object to list
        objects.Add(obj);
    }
    catch (ArgumentException e) {
        Console.WriteLine(e.Message);
    }
}

// print objects
foreach (var obj in objects) {
    Console.WriteLine($"\t{obj}");
}

// get all circles
var circles = objects.OfType<Circle>().ToList();

// calculate all circle intersections
Console.WriteLine("Intersections:");
foreach (var first in circles) {
    var intersections = circles
        .Where(c => c != first && first.IntersectsWith(c))
        .Select(c => c.ShortString()).ToList();
    if (intersections.Count > 0)
        Console.WriteLine($"\t{first} intersects with {string.Join(", ", intersections)}");
}

struct ShapeArgs
{
    public string Type { get; set; }
    public List<(string, float)> Args { get; set; }
}

class Shape
{
    public float X { get; set; }
    public float Y { get; set; }
    public Guid Id { get; } = Guid.NewGuid();

    public Shape() { }

    public Shape(float x, float y) {
        X = x;
        Y = y;
    }

    public virtual float Area => 0;
    public virtual float Perimeter => 0;

    public virtual Shape Parse(List<(string, float)> args) {
        for (int i = args.Count - 1; i >= 0; i--) {
            var (key, value) = args[i];
            if (key == "x") {
                X = value;
                args.RemoveAt(i);
            }
            else if (key == "y") {
                Y = value;
                args.RemoveAt(i);
            }
        }

        return this;
    }

    public static (string, List<(string, float)>) GetArgs(string input) {
        var matches = new Regex(@"^(?<shape>\w+)(\s+(?<arg>\w+)=(?<value>\w+))*\s*$").Match(input);
        if (!matches.Success)
            throw new ArgumentException("Invalid input");

        var type = matches.Groups["shape"].Value;
        var args = matches.Groups["arg"].Captures.Zip(matches.Groups["value"].Captures)
            .Select((values, i) =>
                (values.First.Value, float.Parse(values.Second.Value))).ToList();

        return (type, args);
    }

    public override string ToString() => $"Shape {Id} at ({X}, {Y})";
}

class Circle : Shape
{
    public float Radius { get; set; } = 1;

    public Circle() { }

    public Circle(float x, float y, float radius) : base(x, y) {
        Radius = radius;
    }

    public override float Area => (float)Math.PI * Radius * Radius;
    public override float Perimeter => 2 * (float)Math.PI * Radius;

    // |AB| <= A.r + B.r 
    public bool IntersectsWith(Circle other) =>
        Math.Sqrt((X - other.X) * (X - other.X) + (Y - other.Y) * (Y - other.Y)) <= Radius + other.Radius;

    public override string ToString() =>
        $"Circle {Id} at ({X}, {Y}) with radius {Radius} and area of {Area} and circumference of {Perimeter}";

    public string ShortString() => $"Circle {Id} at ({X}, {Y}) with radius {Radius}";

    public override Circle Parse(List<(string, float)> args) {
        for (int i = args.Count - 1; i >= 0; i--) {
            var (key, value) = args[i];
            if (key is "radius" or "r") {
                Radius = value;
                args.RemoveAt(i);
            }
        }

        base.Parse(args);

        return this;
    }
}

class Rectangle : Shape
{
    public virtual float A { get; set; } = 1;
    public virtual float B { get; set; } = 1;
    
    public override float Area => A * B;
    public override float Perimeter => 2 * (A + B);
    
    public Rectangle() { }
    public Rectangle(float x, float y, float a, float b) : base(x, y) {
        A = a;
        B = b;
    }
    
    public override string ToString() =>
        $"Rectangle {Id} at ({X}, {Y}) with sides {A} and {B} and area of {Area} and perimeter of {Perimeter}";

    public override Rectangle Parse(List<(string, float)> args) {
        for (var i = args.Count - 1; i >= 0; i--) {
            var (key, value) = args[i];
            switch (key) {
                case "a":
                    A = value;
                    args.RemoveAt(i);
                    break;
                case "b":
                    B = value;
                    args.RemoveAt(i);
                    break;
            }
        }

        base.Parse(args);

        return this;
    }

}

class Square : Rectangle
{
    public override float A {
        get => base.A;
        set {
            base.A = value;
            base.B = value;
        }
    }

    public override float B {
        get => base.B;
        set {
            base.A = value;
            base.B = value;
        }
    }

    public override float Area => A * A;
    public override float Perimeter => 4 * A;

    public Square() { }

    public Square(float x, float y, float side) : base(x, y, side, side) {
        A = side;
    }

    public override string ToString() =>
        $"Square {Id} at ({X}, {Y}) with side {A} and area of {Area} and perimeter of {Perimeter}";

    public string ShortString() => $"Square {Id} at ({X}, {Y}) with side {A}";

    public override Square Parse(List<(string, float)> args) {
        for (int i = args.Count - 1; i >= 0; i--) {
            var (key, value) = args[i];
            if (key is "a" or "b") {
                A = value;
                args.RemoveAt(i);
            }
        }

        base.Parse(args);

        return this;
    }
}

static class Helpers
{
    public static string GetStringFromConsole(string prompt) {
        Console.Write(prompt);
        return Console.ReadLine() ?? string.Empty;
    }

    public static int? GetIntFromConsole(string prompt, bool allowEmpty = false) {
        string res;
        int number;
        do {
            Console.Write(prompt);
            res = Console.ReadLine()!;
            if (string.IsNullOrEmpty(res) && allowEmpty)
                return null;
        } while (!int.TryParse(res, out number));

        return number;
    }

    public static float? GetFloatFromConsole(string prompt, bool allowEmpty = false) {
        string res;
        float number;
        do {
            Console.Write(prompt);
            res = Console.ReadLine()!;
            if (string.IsNullOrEmpty(res) && allowEmpty)
                return null;
        } while (!float.TryParse(res, out number));

        return number;
    }

    public static float Random(float a, float b) => System.Random.Shared.NextSingle() * (b - a) + a;

    public static float Random(float a) => Random(0, a);
}