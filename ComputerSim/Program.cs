﻿var devices = new Computer[] {
    new Notebook(OS.Uwuntu, 32, Processor.Intel, 7000),
    new Notebook(OS.Windows, 16, Processor.ARM, 5000),
    new Tablet(OS.Android, 6, Processor.ARM, 4000, false),
    new Tablet(OS.iOS, 6, Processor.ARM, 5000, true),
};

foreach (var groupByOs in devices.GroupBy(x => x.OS)) {
    Console.WriteLine(groupByOs.Key.ToString());
    foreach (var device in groupByOs)
        Console.WriteLine($"\t{device}");
}

enum OS
{
    Android,
    iOS,
    Windows,
    MacOS,
    Uwuntu,
    Linux,
}

enum Processor
{
    Intel,
    AMD,
    ARM,
    MIPS,
    PowerPC,
    SPARC,
}

class Computer
{
    public OS OS { get; set; }
    public int RAM { get; set; }
    public Processor Processor { get; set; }

    public Computer(OS os, int ram, Processor processor) {
        OS = os;
        RAM = ram;
        Processor = processor;
    }

    public override string ToString() => $"Computer<{OS} {RAM}gb {Processor}>";
}

class PortableDevice : Computer
{
    public int BatteryCapacity { get; set; }

    public PortableDevice(OS os, int ram, Processor processor, int batteryCapacity) : base(os, ram, processor) {
        BatteryCapacity = batteryCapacity;
    }
}

class Notebook : PortableDevice
{
    public Notebook(OS os, int ram, Processor processor, int batteryCapacity)
        : base(os, ram, processor, batteryCapacity) { }
}

class Tablet : PortableDevice
{
    public bool Stylus { get; set; }

    public Tablet(OS os, int ram, Processor processor, int batteryCapacity, bool stylus)
        : base(os, ram, processor, batteryCapacity) {
        Stylus = stylus;
    }
}