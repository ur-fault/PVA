﻿Console.WriteLine($"y = x^3; 0 <= x <= 2; steps: 100 = {AreaUnderCurve(val => Math.Pow(val, 3), 0, 2, 100)}");
Console.WriteLine($"y = x^3; 0 <= x <= 2; steps: 1000 = {AreaUnderCurve(val => Math.Pow(val, 3), 0, 2, 1000)}");
Console.WriteLine($"y = x^3; 0 <= x <= 2; steps: 10000 = {AreaUnderCurve(val => Math.Pow(val, 3), 0, 2, 10000)}");

Console.WriteLine($"y = x^2; 2 <= x <= 3; steps: 100 = {AreaUnderCurve(val => Math.Pow(val, 2), 2, 3, 100)}");
Console.WriteLine($"y = x^2; 2 <= x <= 3; steps: 1000 = {AreaUnderCurve(val => Math.Pow(val, 2), 2, 3, 1000)}");
Console.WriteLine($"y = x^2; 2 <= x <= 3; steps: 10000 = {AreaUnderCurve(val => Math.Pow(val, 2), 2, 3, 10000)}");

double AreaUnderCurve(Func<double, double> func, double min, double max, int steps) {
    var area = 0.0;
    for (var i = 0; i < steps; i++) {
        var start = ((i + 0.5) / steps).Map(0, 1, min, max);
        area += func(start) * (max - min) / steps;
    }

    return area;
}

static class Extensions
{
    public static double Map(this double s, double a1, double a2, double b1, double b2) =>
        b1 + (s - a1) * (b2 - b1) / (a2 - a1);
}